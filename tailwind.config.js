/*
 ** TailwindCSS Configuration File
 **
 ** Docs: https://tailwindcss.com/docs/configuration
 ** Default: https://github.com/tailwindcss/tailwindcss/blob/master/stubs/defaultConfig.stub.js
 */
module.exports = {
	mode: 'jit',
	theme: {
		extend: {
			colors: {
				'gfinity-red': '#E94235',
				'electric-blue': '#293894',
				'dark-gray': '#1B1B1B',
				'light-gray': '#6B6B65',
				'concrete-gray': '#CFD1C7',
				'sand-gray': '#DFE0D9',
				'gray-gradient': '#E8E9E3'
			}
		}
	},
	variants: {},
	plugins: []
}
